package com.example.bookstore.controller;

import com.example.bookstore.DTO.UserCredentials;
import com.example.bookstore.model.User;
import com.example.bookstore.repository.UserRepository;
import com.example.bookstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

@Controller
public class ProfileController {
    @Resource
    private UserRepository userRepository;
    @Autowired
    CartService cartService;

    @GetMapping("/profile")
    public String viewUserProfile(Model model){
        model.addAttribute("cartItemCount", cartService.getItemsCount());
        return "profile";
    }

    @PostMapping("/profile/update")
    public String editUserProfile(UserCredentials credentials){
        User userFromDb = userRepository.findById(credentials.getId()).get();
        userFromDb.setUsername(credentials.getUsername());
        userFromDb.setPassword(credentials.getPassword());
        userFromDb.setFullName(credentials.getFullName());
        userFromDb.setAddress(credentials.getAddress());
        userFromDb.setPhone(credentials.getPhone());
        userFromDb.setEmail(credentials.getEmail());
        userRepository.save(userFromDb);
        return "redirect:/profile";
    }
}
