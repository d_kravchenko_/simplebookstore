package com.example.bookstore.DTO;

/**
 * Used to update order status
 */
public class OrderStatus {
    private Long orderId;
    private boolean approved;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public boolean getApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
