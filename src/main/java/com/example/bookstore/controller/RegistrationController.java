package com.example.bookstore.controller;

import com.example.bookstore.model.Role;
import com.example.bookstore.model.User;
import com.example.bookstore.repository.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import java.util.Collections;

@Controller
public class RegistrationController {
    @Resource
    private UserRepository userRepository;

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(User user, Model model){
        User userFromDb = userRepository.findByUsername(user.getUsername());

        if (userFromDb != null) {
            model.addAttribute("message", "User with this name already exists.");
            return "registration";
        }

        user.setRoles(Collections.singleton(Role.USER));
        user.setActive(true);
        user.setFullName("");
        user.setAddress("");
        user.setPhone("");
        user.setEmail("");
        userRepository.save(user);

        return "redirect:/login";
    }
}
