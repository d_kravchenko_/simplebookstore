<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
    <div class="container">
        <h5>Register new user</h5>
        <p style="color:red">${message?ifExists}</p>
        <@l.login "/registration" true />
    </div>
</@c.page>