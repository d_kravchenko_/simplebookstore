package com.example.bookstore.repository;

import com.example.bookstore.model.Cart;
import com.example.bookstore.model.User;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long> {
    Cart findByUser(User user);
}
