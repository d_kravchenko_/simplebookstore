<#import "parts/common.ftl" as c>

<@c.page>
    <#include "parts/navbar.ftl">
    <div class="container" style="margin-top: 50pt">
        <h3>Book catalog: </h3>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Author</th>
                <th scope="col">Genre</th>
                <th scope="col">Price</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <#list books as book>
            <tr>
                <th scope="row">${book?counter}</th>
                <th scope="row">${book.name}</th>
                <th scope="row">${book.author}</th>
                <th scope="row">${book.genre}</th>
                <th scope="row">$${book.price}</th>
                <th scope="row">
                    <form action="view">
                        <input type="hidden" name="id" value="${book.id}">
                        <input type="submit" class="btn btn-primary" value="View">
                    </form>
                </th>
            </tr>
            </#list>
            </tbody>
        </table>
    </div>
</@c.page>