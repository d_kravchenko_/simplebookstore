<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">
<@c.page>
    <div class="container">
        <h1 class="m-3">${book.name}</h1>
        <h3 class="m-3">by ${book.author}</h3>
        <div class="row ml-3">
            <div class="col">
                <img class="mt-3" src="${image}">
            </div>
            <div class="col">
                <p class="ml-4 mt-4"><b>Book Details:</b></p>
                <ul class="ml-2">
                    <li>
                        <p><b>Genre: </b>${book.genre}</p>
                    </li>
                    <li>
                        <p><b>Description: </b>${book.description}</p>
                    </li>
                </ul>
                <div class="row mt-5">
                    <div class="col">
                        <h3>Price: $${book.price}</h3>
                    </div>
                    <div class="col">
                        <div class="row">
                            <#if !isAdmin>
                                <form method="post" action="/addToCart">
                                    <input type="hidden" name="bookId" value="${book.id}">
                                    <input type="hidden" name="_csrf" value="${_csrf.token}">
                                    <input type="submit" class="btn btn-primary" value="Add to cart">
                                </form>
                            </#if>
                            <form class="ml-3" action="/">
                                <input type="submit" class="btn btn-secondary" value="Back">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</@c.page>