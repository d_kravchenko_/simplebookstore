<#import "parts/common.ftl" as c>
<@c.page>
    <div class="container mt-3">
        <#if notInStockList?size == 0>
            <h3>The order successfully created!</h3>
            <p>Please, wait until your order will be approved by the administrator.</p>
            <a href="/orders">View order list</a>
        <#else>
            <h3>Order creation failed :(</h3>
            <div>
                The following books are not available in the requested quantity:
                <ul>
                    <#list notInStockList as item>
                        <li>${item.name} (${item.count} left in stock)</li>
                    </#list>
                </ul>
            </div>
            <a href="/cart">Return</a>
        </#if>
    </div>
</@c.page>