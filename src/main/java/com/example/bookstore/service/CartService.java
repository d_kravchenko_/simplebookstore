package com.example.bookstore.service;

import com.example.bookstore.model.Book;
import com.example.bookstore.model.Cart;
import com.example.bookstore.model.CartItem;
import com.example.bookstore.model.Order;
import com.example.bookstore.model.OrderItem;
import com.example.bookstore.model.User;
import com.example.bookstore.repository.CartItemRepository;
import com.example.bookstore.repository.CartRepository;
import com.example.bookstore.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CartService {

    @Autowired
    private AuthService authService;
    @Resource
    private CartRepository cartRepository;
    @Resource
    private CartItemRepository cartItemRepository;
    @Resource
    private OrderRepository orderRepository;

    /**
     * Returns saved cart instance or creates an empty one if it doesn't exist
     * @return cart
     */
    public Cart getCart() {
        User user = authService.getLoginedUser();
        Cart cart = cartRepository.findByUser(user);
        if (cart == null){
            cart = new Cart();
            cart.setUser(user);
            cart.setItems(new HashSet<>());
            cartRepository.save(cart);
        }
        return cart;
    }

    /**
     * Returns total price of all books in cart
     * @return total price
     */
    public double getTotalPrice(){
        AtomicReference<Double> totalPrice = new AtomicReference<>(0.0);
        getCart().getItems().forEach(cartItem -> totalPrice.updateAndGet(v -> v + cartItem.getBook().getPrice() * cartItem.getQuantity()));
        return totalPrice.get();
    }

    /**
     * Returns the number of books in cart
     * @return quantity
     */
    public int getItemsCount(){
        AtomicReference<Integer> quantity = new AtomicReference<>(0);
        getCart().getItems().forEach(cartItem -> quantity.updateAndGet(v -> v + cartItem.getQuantity()));
        return quantity.get();
    }

    /**
     * Adds a new cart item to the cart or increases its quantity by 1 if a cart item with this book already exists
     * @param book
     */
    public void addItem(Book book){
        Cart cart = getCart();
        List<CartItem> cartItemList = cart.getItems().stream().filter(item -> item.getBook().equals(book)).collect(Collectors.toList());
        CartItem cartItem;
        if (!cartItemList.isEmpty()){
            cartItem = cartItemList.get(0);
            cartItem.setQuantity(cartItem.getQuantity() + 1);
        }else {
            cartItem = new CartItem();
            cartItem.setBook(book);
            cartItem.setQuantity(1);
        }
        cartItem.setCart(cart);
        cart.getItems().add(cartItem);
        cartRepository.save(cart);
    }

    /**
     * Removes an existing cart item or decreases its quantity by 1 if it is greater than 1
     * @param cartItemId
     */
    public void removeItem(Long cartItemId){
        CartItem cartItem = cartItemRepository.findById(cartItemId).get();
        Cart cart = cartItem.getCart();
        if (cartItem.getQuantity() > 1){
            cartItem.setQuantity(cartItem.getQuantity() - 1);
            cartItemRepository.save(cartItem);
        }else {
            cart.getItems().remove(cartItem);
            cartItemRepository.delete(cartItem);
            cartRepository.save(cart);
        }
    }

    /**
     * Creates an order and wipes the cart contents.
     * @return List of books that are not available in requested quantity
     */
    public List<Book> checkout(){
        List<Book> notInStockList = new ArrayList<>();
        Cart cart = getCart();
        Order order = new Order();
        order.setUser(cart.getUser());
        order.setDate(Date.from(Instant.now()));
        order.setPrice(getTotalPrice());
        order.setApproved(false);
        Set<OrderItem> orderItems = new HashSet<>();
        for (CartItem item : cart.getItems()){
            // check if the requested number of books is available
            if (item.getQuantity() > item.getBook().getCount()){
                notInStockList.add(item.getBook());
            }else {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrder(order);
                orderItem.setBook(item.getBook());
                orderItem.setQuantity(item.getQuantity());
                // reduce the number of books in stock by requested quantity
                orderItem.getBook().setCount(orderItem.getBook().getCount() - orderItem.getQuantity());
                orderItems.add(orderItem);
            }
        }
        order.setItems(orderItems);

        // save result if all the requested books are available
        if (notInStockList.isEmpty()) {
            orderRepository.save(order);
            cartRepository.delete(cart);
        }

        return notInStockList;
    }
}
