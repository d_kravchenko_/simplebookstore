<#include "security.ftl">
<nav class="navbar fixed-top navbar-expand-lg navbar-light scrolling-navbar" style="background-color: #e3f2fd;">
    <div class="container">
        <a href="/" class="navbar-brand">
            <strong>Simple Book Store</strong>
        </a>
        <form action="/" method="get" class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" name="filter" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
        <ul class="navbar-nav">
            <li class="ml-4">${name}</li>
            <#if isAdmin>
                <li class="ml-4">
                   <a href="/admin">Admin page</a>
                </li>
            <#else>
                <li class="ml-4">
                    <a href="/cart">Cart (${cartItemCount})</a>
                </li>
                <li class="ml-4">
                    <a href="/orders">Orders</a>
                </li>
                <li class="ml-4">
                    <a href="/profile">Profile</a>
                </li>
            </#if>
            <li class="ml-4">
                <form action="/logout" method="post" id="logout_form">
                    <input type="hidden" name="_csrf" value="${_csrf.token}" />
                    <a href="javascript:{}" onclick="document.getElementById('logout_form').submit();">Sign Out</a>
                </form>
            </li>
        </ul>
    </div>
</nav>
