package com.example.bookstore.repository;

import com.example.bookstore.model.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long> {
    @Query("from Book b where b.name like LOWER(CONCAT ('%', :filter, '%')) " +
            "or b.author like LOWER(CONCAT ('%', :filter, '%')) " +
            "or b.genre like LOWER(CONCAT ('%', :filter, '%'))")
    List<Book> findFiltered(@Param("filter") String filter);
}
