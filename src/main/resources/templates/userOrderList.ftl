<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page>
    <#include "parts/navbar.ftl">
    <div class="container" style="margin-top: 50pt">
        <h3>${name}'s orders</h3>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Content</th>
                <th scope="col">Price</th>
                <th scope="col">Approved</th>
            </tr>
            </thead>
            <tbody>
            <#list orders as order>
                <tr>
                    <th scope="row">${order?counter}</th>
                    <th scope="row">${order.getFormattedDate()}</th>
                    <th scope="row">
                        <#list order.items as item>
                        <ul>
                            <li>${item.getBook().name} by ${item.getBook().author} (${item.quantity})</li>
                        </ul>
                        </#list>
                    </th>
                    <th scope="row">${order.price}</th>
                    <th scope="row">
                        <#if order.approved>
                        <strong>Yes</strong>
                        <#else>
                        <strong>No</strong>
                        </#if>
                    </th>
                </tr>
            </#list>
            </tbody>
        </table>
    </div>
</@c.page>