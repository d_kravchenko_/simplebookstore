<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page>
    <#include "parts/navbar.ftl">
    <div class="container" style="margin-top: 50pt">
        <h3>Orders:</h3>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">User</th>
                <th scope="col">Content</th>
                <th scope="col">Price</th>
                <th scope="col">Approved</th>
            </tr>
            </thead>
            <tbody>
            <#list orders as order>
                <tr>
                    <th scope="row">${order?counter}</th>
                    <th scope="row">${order.getFormattedDate()}</th>
                    <th scope="row">${order.user.username}</th>
                    <th scope="row">
                        <#list order.items as item>
                            <ul>
                                <li>${item.getBook().name} by ${item.getBook().author} (${item.quantity})</li>
                            </ul>
                        </#list>
                    </th>
                    <th scope="row">${order.price}</th>
                    <th scope="row">
                        <form action="/admin/approve" method="post" id="checkbox_form_${order.id}">
                            <#assign isChecked = order.approved >
                            <input type="checkbox" name="approved" <#if isChecked >checked</#if> value="${isChecked?then('false','true')}" onchange="document.getElementById('checkbox_form_${order.id}').submit();">
                            <input type="hidden" name="orderId" value="${order.id}">
                            <input type="hidden" name="_csrf" value="${_csrf.token}">
                        </form>
                    </th>
                </tr>
            </#list>
            </tbody>
        </table>
    </div>
</@c.page>