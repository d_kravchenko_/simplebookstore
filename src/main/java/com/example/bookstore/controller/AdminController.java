package com.example.bookstore.controller;

import com.example.bookstore.DTO.OrderStatus;
import com.example.bookstore.model.Order;
import com.example.bookstore.repository.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

@Controller
public class AdminController {
    @Resource
    private OrderRepository orderRepository;

    @GetMapping("/admin")
    public String openAdminPage(Model model){
        model.addAttribute("orders", orderRepository.findAllByOrderByApprovedAscDateDesc());
        return "adminPage";
    }

    @PostMapping("/admin/approve")
    public String approveOrder(OrderStatus status){
        Order order = orderRepository.findById(status.getOrderId()).get();
        order.setApproved(status.getApproved());
        orderRepository.save(order);
        return "redirect:/admin";
    }

}
