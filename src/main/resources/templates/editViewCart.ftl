<#import "parts/common.ftl" as c>
<@c.page>
    <#include "parts/navbar.ftl">
    <div class="container" style="margin-top: 50pt">
        <h3>${name}'s cart</h3>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Author</th>
                <th scope="col">Genre</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <#list cart.items as item>
                <tr>
                    <th scope="row">${item?counter}</th>
                    <th scope="row">${item.book.name}</th>
                    <th scope="row">${item.book.author}</th>
                    <th scope="row">${item.book.genre}</th>
                    <th scope="row">$${item.book.price}</th>
                    <th scope="row">
                        <p>${item.quantity}</p>
                    </th>
                    <th scope="row">
                        <div class="row">
                            <form class="pr-3" action="/addToCart" method="post">
                                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                                <input type="hidden" name="bookId" value="${item.book.id}">
                                <input type="submit" class="btn btn-primary" value="+">
                            </form>
                            <form action="/removeFromCart" method="post">
                                <input type="hidden" name="_csrf" value="${_csrf.token}" />
                                <input type="hidden" name="itemId" value="${item.id}">
                                <input type="submit" class="btn btn-primary" value="-">
                            </form>
                        </div>
                    </th>
                </tr>
            </#list>
            </tbody>
        </table>

        <#if cart.hasItems()>
                <div class="row">
                    <h4>Total price: $${totalPrice}</h4>
                    <form class="ml-5" action="/cart/placeOrder" method="post">
                        <input type="hidden" name="_csrf" value="${_csrf.token}" />
                        <input type="hidden" name="cart_id" value="${cart.id}">
                        <input type="submit" class="btn btn-primary" value="Place order">
                    </form>
                </div>
        </#if>
    </div>
</@c.page>