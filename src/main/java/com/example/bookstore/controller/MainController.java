package com.example.bookstore.controller;

import com.example.bookstore.model.Book;
import com.example.bookstore.repository.BookRepository;
import com.example.bookstore.repository.OrderRepository;
import com.example.bookstore.service.AuthService;
import com.example.bookstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Base64;
import java.util.List;

@Controller
public class MainController {
    @Resource
    private BookRepository bookRepository;
    @Resource
    private OrderRepository orderRepository;
    @Autowired
    private AuthService authService;
    @Autowired
    CartService cartService;

    @GetMapping("/")
    public String filter(
            @RequestParam(value = "filter", required = false) String filter,
            Model model
    ){
        List<Book> books;
        if (filter == null || filter.isEmpty()){
            books = (List<Book>) bookRepository.findAll();
        }else{
            books = bookRepository.findFiltered(filter);
        }

        model.addAttribute("books", books);
        model.addAttribute("cartItemCount", cartService.getItemsCount());
        return "main";
    }

    @GetMapping("/view")
    public String view(@RequestParam("id") String id, Model model){
        Book book = bookRepository.findById(Long.parseLong(id)).get();
        byte[] imgBytesAsBase64 = Base64.getEncoder().encode(book.getImage());
        String imgDataAsBase64 = new String(imgBytesAsBase64);
        String imgAsBase64 = "data:image/jpg;base64," + imgDataAsBase64;
        model.addAttribute("book", book);
        model.addAttribute("image", imgAsBase64);
        return "view";
    }

    @GetMapping("/orders")
    public String openUserOrderList(Model model){
        model.addAttribute("orders", orderRepository.findByUserOrderByDateDesc(authService.getLoginedUser()));
        model.addAttribute("cartItemCount", cartService.getItemsCount());
        return "userOrderList";
    }
}
