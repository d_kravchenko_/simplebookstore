package com.example.bookstore.controller;

import com.example.bookstore.model.Book;
import com.example.bookstore.repository.BookRepository;
import com.example.bookstore.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class CartController {
    @Autowired
    private CartService cartService;
    @Resource
    private BookRepository bookRepository;

    @GetMapping("/cart")
    public String editViewCart(Model model){
        model.addAttribute("cart", cartService.getCart());
        model.addAttribute("totalPrice", cartService.getTotalPrice());
        model.addAttribute("cartItemCount", cartService.getItemsCount());
        return "editViewCart";
    }

    @PostMapping("/cart/placeOrder")
    public String placeOrder(@RequestParam("cart_id") Long cartId, Model model){
        List<Book> notInStockList = cartService.checkout();
        model.addAttribute("notInStockList", notInStockList);
        return "orderCreated";
    }

    @PostMapping("/addToCart")
    public String addToCart(@RequestParam("bookId") Long bookId){
        Book book = bookRepository.findById(bookId).get();
        cartService.addItem(book);
        return "redirect:/cart";
    }

    @PostMapping("/removeFromCart")
    public String removeFromCart(@RequestParam("itemId") Long itemId){
        cartService.removeItem(itemId);
        return "redirect:/cart";
    }
}
