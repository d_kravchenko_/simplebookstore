package com.example.bookstore.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;

@Entity
public class Cart extends AbstractPersistable<Long> {
    @OneToOne
    private User user;

    @OneToMany(mappedBy="cart", cascade = CascadeType.ALL)
    private Set<CartItem> items;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<CartItem> getItems() {
        return items;
    }

    public void setItems(Set<CartItem> items) {
        this.items = items;
    }

    public boolean hasItems(){
        return items != null && !items.isEmpty();
    }
}
