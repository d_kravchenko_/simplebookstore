<#import "parts/common.ftl" as c>
<#include "parts/security.ftl">

<@c.page>
    <#include "parts/navbar.ftl">
    <div class="container" style="margin-top: 50pt">
        <h3>${name}'s profile data</h3>
        <form action="/profile/update" method="post">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Login:</label>
                <div class="col-sm-6">
                    <input type="text" readonly="readonly" name="username" class="form-control"  value="${user.username}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password:</label>
                <div class="col-sm-6">
                    <input type="password" name="password" class="form-control"  value="${user.password}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Full name:</label>
                <div class="col-sm-6">
                    <input type="text" name="fullName" class="form-control"  value="${user.fullName}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Address:</label>
                <div class="col-sm-6">
                    <input type="text" name="address" class="form-control"  value="${user.address}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Phone:</label>
                <div class="col-sm-6">
                    <input type="text" name="phone" class="form-control"  value="${user.phone}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">E-mail:</label>
                <div class="col-sm-6">
                    <input type="text" name="email" class="form-control"  value="${user.email}" />
                </div>
            </div>
            <input type="hidden" name="id" value="${user.id}" />
            <input type="hidden" name="_csrf" value="${_csrf.token}" />
            <input type="submit" class="btn btn-primary" value="Update profile" />
        </form>
    </div>
</@c.page>